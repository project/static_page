<?php

namespace Drupal\Tests\static_page\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Test basic module functionality: settings form, etc.
 *
 * @group static_page
 */
class StaticPageTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'static_page',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->user = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($this->user);

    // Create node types.
    $node_types = [
      'article' => t('Article'),
      'page' => t('Basic page'),
      'static_page' => t('Static page'),
    ];
    foreach ($node_types as $id => $label) {
      $type = $this->drupalCreateContentType(['type' => $id, 'name' => $label]);
      node_add_body_field($type);
    }

    FieldStorageConfig::create([
      'field_name' => 'static_page_field',
      'entity_type' => 'node',
      'type' => 'text_long',
    ])->save();
    FieldConfig::create([
      'field_name' => 'static_page_field',
      'entity_type' => 'node',
      'bundle' => 'static_page',
    ])->save();
  }

  /**
   * Tests the Static Page settings form.
   */
  public function testSettingsForm() {
    $this->drupalGet(Url::fromRoute('<front>'));
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet(Url::fromRoute('static_page.settings'));
    $this->assertSession()->statusCodeEquals(200);
    $button = $this->getSession()->getPage()->findButton('Save configuration');
    $this->assertNotEmpty($button);
    $options = $this->xpath('//select[@name="fields[article]"]/option');
    $this->assertCount(2, $options);
    $this->assertSession()->optionExists('fields[article]', 'body');
    $this->assertSession()->optionExists('fields[article]', '-- None --');
    $options = $this->xpath('//select[@name="fields[page]"]/option');
    $this->assertCount(2, $options);
    $this->assertSession()->optionExists('fields[page]', 'body');
    $this->assertSession()->optionExists('fields[page]', '-- None --');
    $options = $this->xpath('//select[@name="fields[static_page]"]/option');
    $this->assertCount(3, $options);
    $this->assertSession()->optionExists('fields[static_page]', 'body');
    $this->assertSession()->optionExists('fields[static_page]', 'static_page_field');
    $this->assertSession()->optionExists('fields[static_page]', '-- None --');
    $edit = [
      'fields[article]' => '',
      'fields[page]' => '',
      'fields[static_page]' => 'static_page_field',
    ];
    $this->submitForm($edit, 'Save configuration');
  }

}
